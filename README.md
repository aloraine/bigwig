# Bigwig and bigbed parsing #

This repository contains code for reading Bigbed and Bigwig format files. 
The code is from an earlier version of IGV, repackaged as an OSGi bundle for
use in other projects.

### To build ###

* Install gradle
* Execute `gradle jar` to create the artifacts

### To deploy to a maven repository ###

* Edit `build.gradle` with address of target repository and credentials
* Execute `gradle uploadArchives`

This will deploy compiled artifact and source code.

### Contribution guidelines ###

* Fork this repository
* Make changes on a topic branch
* Submit a pull request from topic branch to master branch 

### Who do I talk to? ###

* Kiran Korey 

or

* Ann Loraine (aloraine@uncc.edu)

